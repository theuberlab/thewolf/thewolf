package e2eTests

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	g "github.com/onsi/ginkgo"
	o "github.com/onsi/gomega"

	"github.com/prometheus/alertmanager/template"

	"bitbucket.org/theuberlab/lug"
)

var (
	useragent = "Majordomo e2e testing"
)

var _ = g.Describe("[Feature:webhooks][CI]", func() {

	g.Describe("Requests to the /webhooks endpoint", func() {

		g.BeforeEach(func() {

			//TODO: Figure out how to compile and run the app.
			// Or perhaps we should just do that in the pipeline. Compile, build a docker image, run?
		})

		g.Describe("For a GET request", func() {
			g.It("Should respond to a get with a coming soon message", func() {

				//e2e.Logf("Go version: %s", runtime.Version())
				//TODO: Come up with a better way to handle this.
				httpClient := &http.Client{
					Transport:
					&http.Transport{
						//Proxy: http.ProxyURL(proxyurl),
						TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
					},
				}

				url := "https://localhost:8443/webhook"
				//e2e.Logf("Calling URL: %s\n", url)

				req, _ := http.NewRequest("GET", url, nil)
				req.Header.Set("Accept", "application/json")
				req.Header.Set("User-Agent", useragent)

				// Actually perform the request.
				resp, err := httpClient.Do(req)

				if err != nil {
					lug.Error("Message", "Request failed with error", "ERROR", err.Error())
					g.Fail(fmt.Sprintf("Failed with ERROR: %v", err.Error()))
				}

				defer resp.Body.Close()

				var bodystring string
				if resp.StatusCode == http.StatusOK {

					bodyBytes, _ := ioutil.ReadAll(resp.Body)

					bodystring = string(bodyBytes)
					lug.Error("Message", "Got response body", "Body", fmt.Sprintf("%v", bodystring))
				}

				lug.Debug("Message", "Dumping request")
				o.Expect(bodystring).To(o.Equal("Nothing here yet but in the future we will tell you how to format a POST to this same endpoint."))
			})
		})


		g.Describe("For POST requests", func() {
			g.Describe("For a valid request and a configured alert.", func() {
				g.It(fmt.Sprintf("Should return a 200 ok"), func() {
					//TODO: Provably easier to just add the body as a sting for these curl examples
					//curl -kv -H "Content-Type: application/json" -X POST https://localhost:8443/webhook --data '{"receiver": "webhook","status": "firing","alerts": [{"status": "firing","labels": {"team": "platform","severity": "Critical","alertname": "NodeNotReadyForTooLongJustOne"},"annotations": {"description": "Node ip-10.1.2.3.us-west-2.compute.internal has been in a NOT READY state for more than 9m, this should be investigated.","summary": "Node ip-10.1.2.3.us-west-2.compute.internal has been in a NOT READY state for more than 9m, thats generally not awesome."},"startsAt": "2018-08-03T09:52:26.739266876+02:00","endsAt": "0001-01-01T00:00:00Z","generatorURL": "http://simon-laptop:9090/graph?g0.expr=go_memstats_alloc_bytes+%3E+0\u0026g0.tab=1"}]}'

					var postBody *template.Data

					postBody = &template.Data{}

					labels := make(map[string]string)

					labels["team"] = "CodeMonkeys"
					labels["severity"] = "Critical"
					labels["alertname"] = "NodeNotReadyForTooLongJustOne"

					annotations := make(map[string]string)

					labels["description"] = "Node ip-101-102-103-104.us-west-2.compute.internal has been in a NOT READY state for more than 9m, this should be investigated."
					annotations["summary"] = "Node ip-101-102-103-104.us-west-2.compute.internal has been in a NOT READY state for more than 9m, that's generally not awesome."

					startsAt, err := time.Parse(time.RFC3339, "2018-08-03T09:52:26.739266876+02:00")
					if err != nil {
						lug.Error("Message", "Failed to parse StartsAt datetime", "Error", err)
					}

					endsAt, err := time.Parse(time.RFC3339, "0001-01-01T00:00:00Z")
					if err != nil {
						lug.Error("Message", "Failed to parse EndsAt datetime", "Error", err)
					}


					postBody = &template.Data{
						Receiver: "webhook",
						Status:   "firing",
						Alerts: []template.Alert{
							{
								Status:       "firing",
								Labels:       labels,
								Annotations:  labels,
								StartsAt:     startsAt,
								EndsAt:       endsAt,
								GeneratorURL: "http://simon-laptop:9090/graph?g0.expr=go_memstats_alloc_bytes+%3E+0\u0026g0.tab=1",
							},
						},
					}

					lug.Debug("Message", "Post body created", "Body", fmt.Sprintf("%v", postBody))

					//TODO: Make the actual post here


					httpClient := &http.Client{
						Transport:
						&http.Transport{
							//Proxy: http.ProxyURL(proxyurl),
							TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
						},
					}

					url := "https://localhost:8443/webhook"
					//e2e.Logf("Calling URL: %s\n", url)

					jsonBytes, err := json.Marshal(postBody)
					if err != nil {
						lug.Error("Message", "Failed to marshal alert to JSON", "ERROR", err.Error())
						g.Fail(fmt.Sprintf("Failed to marshal alert to JSON with ERROR: %v", err.Error()))
					}

					req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonBytes))
					req.Header.Set("Accept", "application/json")
					req.Header.Set("User-Agent", useragent)


					// Actually perform the request.
					resp, err := httpClient.Do(req)

					if err != nil {
						lug.Error("Message", "Request failed with error", "ERROR", err.Error())
						g.Fail(fmt.Sprintf("Failed with ERROR: %v", err.Error()))
					}

					defer resp.Body.Close()

					var bodystring string
					if resp.StatusCode == http.StatusOK {

						bodyBytes, _ := ioutil.ReadAll(resp.Body)

						bodystring = string(bodyBytes)
						lug.Error("Message", "Got response body", "Body", fmt.Sprintf("%v", bodystring))
					}


					o.Expect(resp.StatusCode).To(o.Equal(http.StatusOK))
				})

			})

			g.Describe("For a valid request but an unconfigured alert", func() {
				g.It(fmt.Sprintf("Should return a 404"), func() {
					//curl -kv -H "Content-Type: application/json" -X POST https://localhost:8443/webhook --data '{"receiver": "webhook","status": "firing","alerts": [{"status": "firing","labels": {"team": "platform","severity": "Critical","alertname": "YouDontKnowThisAlertButItKnowsYou"},"annotations": {"description": "Node ip-10.1.2.3.us-west-2.compute.internal has been in a NOT READY state for more than 9m, this should be investigated.","summary": "Node ip-10.1.2.3.us-west-2.compute.internal has been in a NOT READY state for more than 9m, thats generally not awesome."},"startsAt": "2018-08-03T09:52:26.739266876+02:00","endsAt": "0001-01-01T00:00:00Z","generatorURL": "http://simon-laptop:9090/graph?g0.expr=go_memstats_alloc_bytes+%3E+0\u0026g0.tab=1"}]}'

					//postBody := `{"receiver": "webhook","status": "firing","alerts": [{"status": "firing","labels": {"team": "platform","severity": "Critical","alertname": "YouDontKnowThisAlertButItKnowsYou"},"annotations": {"description": "Node ip-10.1.2.3.us-west-2.compute.internal has been in a NOT READY state for more than 9m, this should be investigated.","summary": "Node ip-10.1.2.3.us-west-2.compute.internal has been in a NOT READY state for more than 9m, thats generally not awesome."},"startsAt": "2018-08-03T09:52:26.739266876+02:00","endsAt": "0001-01-01T00:00:00Z","generatorURL": "http://simon-laptop:9090/graph?g0.expr=go_memstats_alloc_bytes+%3E+0\u0026g0.tab=1"}]}`

					var postBody *template.Data

					postBody = &template.Data{}

					labels := make(map[string]string)

					labels["team"] = "CodeMonkeys"
					labels["severity"] = "Critical"
					labels["alertname"] = "YouDontKnowThisAlertButItKnowsYou"

					annotations := make(map[string]string)

					labels["description"] = "Node ip-101-102-103-104.us-west-2.compute.internal has been in a NOT READY state for more than 9m, this should be investigated."
					annotations["summary"] = "Node ip-101-102-103-104.us-west-2.compute.internal has been in a NOT READY state for more than 9m, that's generally not awesome."

					startsAt, err := time.Parse(time.RFC3339, "2018-08-03T09:52:26.739266876+02:00")
					if err != nil {
						lug.Error("Message", "Failed to parse StartsAt datetime", "Error", err)
					}

					endsAt, err := time.Parse(time.RFC3339, "0001-01-01T00:00:00Z")
					if err != nil {
						lug.Error("Message", "Failed to parse EndsAt datetime", "Error", err)
					}


					postBody = &template.Data{
						Receiver: "webhook",
						Status:   "firing",
						Alerts: []template.Alert{
							{
								Status:       "firing",
								Labels:       labels,
								Annotations:  labels,
								StartsAt:     startsAt,
								EndsAt:       endsAt,
								GeneratorURL: "http://simon-laptop:9090/graph?g0.expr=go_memstats_alloc_bytes+%3E+0\u0026g0.tab=1",
							},
						},
					}


					httpClient := &http.Client{
						Transport:
						&http.Transport{
							//Proxy: http.ProxyURL(proxyurl),
							TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
						},
					}

					url := "https://localhost:8443/webhook"
					//e2e.Logf("Calling URL: %s\n", url)

					jsonBytes, err := json.Marshal(postBody)
					if err != nil {
						lug.Error("Message", "Failed to marshal alert to JSON", "ERROR", err.Error())
						g.Fail(fmt.Sprintf("Failed to marshal alert to JSON with ERROR: %v", err.Error()))
					}

					req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonBytes))
					req.Header.Set("Accept", "application/json")
					req.Header.Set("User-Agent", useragent)


					// Actually perform the request.
					resp, err := httpClient.Do(req)

					if err != nil {
						lug.Error("Message", "Request failed with error", "ERROR", err.Error())
						g.Fail(fmt.Sprintf("Failed with ERROR: %v", err.Error()))
					}

					defer resp.Body.Close()

					var bodystring string

					if resp.StatusCode == http.StatusOK {

						bodyBytes, _ := ioutil.ReadAll(resp.Body)

						bodystring = string(bodyBytes)
						lug.Error("Message", "Got response body", "Body", fmt.Sprintf("%v", bodystring))
					} else {

						bodyBytes, _ := ioutil.ReadAll(resp.Body)

						bodystring = string(bodyBytes)

						lug.Error("Message", "Response body", "Body", fmt.Sprintf("%v", bodystring))
					}

					o.Expect(resp.StatusCode).To(o.Equal(http.StatusNotFound))
				})


			})

			g.Describe("For an invalid request", func() {
				g.It(fmt.Sprintf("Should return an 400"), func() {
					//curl -kv -H "Content-Type: application/json" -X POST https://localhost:8443/webhook --data '{"receiver": "webhook","status": "firing","alerts": [{"status": "firing","labels": {"team": "platform","severity": "Critical","alertname": "NodeNotReadyForTooLongJustOne"},"annotations": {"description": "Node ip-10.1.2.3.us-west-2.compute.internal has been in a NOT READY state for more than 9m, this should be investigated.","summary": "Node ip-10.1.2.3.us-west-2.compute.internal has been in a NOT READY state for more than 9m, thats generally not awesome.",startsAt: "2018-08-03T09:52:26.739266876+02:00","endsAt": "0001-01-01T00:00:00Z",generatorURL": "http://simon-laptop:9090/graph?g0.expr=go_memstats_alloc_bytes+%3E+0\u0026g0.tab=1"}]}'

					postBody := `{"receiver": "webhook","status": "firing","alerts": [{"status": "firing","labels": {"team": "platform","severity": "Critical","alertname": "NodeNotReadyForTooLongJustOne"},"annotations": {"description": "Node ip-10.1.2.3.us-west-2.compute.internal has been in a NOT READY state for more than 9m, this should be investigated.","summary": "Node ip-10.1.2.3.us-west-2.compute.internal has been in a NOT READY state for more than 9m, thats generally not awesome.",startsAt: "2018-08-03T09:52:26.739266876+02:00","endsAt": "0001-01-01T00:00:00Z",generatorURL": "http://simon-laptop:9090/graph?g0.expr=go_memstats_alloc_bytes+%3E+0\u0026g0.tab=1"}]}`
					httpClient := &http.Client{
						Transport:
						&http.Transport{
							//Proxy: http.ProxyURL(proxyurl),
							TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
						},
					}

					url := "https://localhost:8443/webhook"
					//e2e.Logf("Calling URL: %s\n", url)



					jsonBytes, err := json.Marshal(postBody)
					if err != nil {
						lug.Error("Message", "Failed to marshal alert to JSON", "ERROR", err.Error())
						g.Fail(fmt.Sprintf("Failed to marshal alert to JSON with ERROR: %v", err.Error()))
					}

					req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonBytes))
					req.Header.Set("Accept", "application/json")
					req.Header.Set("User-Agent", useragent)


					// Actually perform the request.
					resp, err := httpClient.Do(req)

					if err != nil {
						lug.Error("Message", "Request failed with error", "ERROR", err.Error())
						g.Fail(fmt.Sprintf("Failed with ERROR: %v", err.Error()))
					}

					defer resp.Body.Close()

					var bodystring string

					if resp.StatusCode == http.StatusOK {

						bodyBytes, _ := ioutil.ReadAll(resp.Body)

						bodystring = string(bodyBytes)
						lug.Error("Message", "Got response body", "Body", fmt.Sprintf("%v", bodystring))
					} else {

						bodyBytes, _ := ioutil.ReadAll(resp.Body)

						bodystring = string(bodyBytes)

						lug.Error("Message", "Response body", "Body", fmt.Sprintf("%v", bodystring))
					}


					o.Expect(resp.StatusCode).To(o.Equal(http.StatusBadRequest))
				})

			})
		})
	})
})

//TODO: Make a request that looks like this:
//curl --header "Content-Type: application/json"   --request POST http://localhost:8443/webhook --data '{
//	"receiver": "webhook",
//	"status": "firing",
//	"alerts": [
//		{
//			"status": "firing",
//			"labels": {
//				"team": "platform",
//				"severity": "Critical",
//				"alertname": "NodeNotReadyForTooLongJustOne"
//			},
//			"annotations": {
//				"description": "Node ip-10.1.2.3.us-west-2.compute.internal has been in a NOT READY state for more than 9m, this should be investigated.",
//				"summary": "Node ip-10.1.2.3.us-west-2.compute.internal has been in a NOT READY state for more than 9m, that's generally not awesome."
//			},
//			"startsAt": "2018-08-03T09:52:26.739266876+02:00",
//			"endsAt": "0001-01-01T00:00:00Z",
//			"generatorURL": "http://simon-laptop:9090/graph?g0.expr=go_memstats_alloc_bytes+%3E+0\u0026g0.tab=1"
//		}
//	]
//}'
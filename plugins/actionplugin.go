package plugins

import (
	"time"
	"bitbucket.org/theuberlab/thewolf/workflow"
)

type ActionPlugin interface {
	WolfPlugin
	TriggerAction(ActionInstance) 	string 						// This is stupid but it's v0.0.1 ultimately we should probably have a GetTriggerFunction() Which returns a reference to a function like GetConfigValidator() and GetFailureFunction()
	GetActionConfigValidator() 		func(interface{}) error 	// Get the function which will be used to validat the 'Config' of ActionConfig elements which reference this plugin.
	GetFailureAction() 				func(interface{}) 			// return a reference to the configured onFailure action so that thewolf can trigger that.
}

//TODO: Need to come up with a mechanism for ensuring that plugin names are unique. Possibly this is just a com.bla.blah.convetion.

// The global definition of an action config
type ActionConfig struct {
	//Name			string 			`yaml:"Name"` // Will need this if we switch the format of the config file. Perhaps something else. We need a unique value so that the listener knows which action to call.
	Plugin 			string			`yaml:"Plugin"` // A reference to a specific plugin which must also be defined in the plugins section
	Alert        	string 			`yaml:"Alert"` //TODO: this is going to need to transform into something like 'trigger' or something.
	Concurrency  	string 			`yaml:"Concurrency,omitempty"` // Might just drop this until we have an actual workflow. Or perhaps this should be defined by individual plugins? No I hate that answer.
	Config 			interface{}		`yaml:"Config"` // This will be defined by the plugin itself.
	Comments 		string	 		`yaml:"Comments,omitempty"` // Human readable comments so we can have this in json format if we want and still have comments. Also once the config is in a DB these will be set and fetched by some config api call so the object will need a native way to store comments..
	Timeout 		int				`yaml:"Timeout,omitempty"` // How long to give the Action, in seconds, to complete before assuming it died. NOTE: Depending upon how the plugin functions we may not _actually_ be able to kill it if we kill the thread executing the action plugin.
}

// A plugin configuration
type PluginConfig struct {
	Name 			string 			`yaml:"Name"` // The name of the plugin
	Version 		string 			`yaml:"Version"` //TODO: Make this a type which parses as a syntactic version.
	Comments 		string	 		`yaml:"Comments,omitempty"` // Human readable comments.
}

// The package sent to a plugin when it is called. This includes the full config (for timeout, etc.) as well as the labels collected from the request.
type ActionInstance struct {
	//ID 			string 				// Will probably at some point early on want to do this.
	Config 			ActionConfig 		// The full configuration for this action
	Job 			workflow.Job			// An undefined object that could be anything. I need a MUCH better way to do this but given something like a prometheus alert where labels and annotations could potentially collide I'm not suer how.
	Labels 			map[string]string 	// Key value pairs expected by the action plugin. NOTE: These are NOT prometheus labels, we have our own labels and any plugin might add to them.
	StartTime 		time.Time
}

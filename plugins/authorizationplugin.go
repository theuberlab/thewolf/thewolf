package plugins

import (
	"net/http"
)

type AuthorizationPlugin interface {
	WolfPlugin
	GetAuthorizationFunction() 												func(http.HandlerFunc) http.HandlerFunc
	GetAuthorizationFunctionWithConfig(config AuthorizationPluginConfig) 	func(http.HandlerFunc) http.HandlerFunc
}

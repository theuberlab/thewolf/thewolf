package plugins

import (
	"io/ioutil"
	"os"
	"plugin"

	"bitbucket.org/theuberlab/lug"
)

type PluginManager struct {
	actionPlugins 		map[string]ActionPlugin               	// Actually implemented
	authSourcePlugins	map[string]AuthenticationSourcePlugin 	// In progress
	authPlugins 		map[string]AuthorizationPlugin
	authpluginOrder 	*[]string 								// Sketchy at best
}


func (p *PluginManager) InitPlugins(plugindir string) {

	p.actionPlugins 		= make(map[string]ActionPlugin)
	p.authSourcePlugins		= make(map[string]AuthenticationSourcePlugin)
	p.authPlugins 			= make(map[string]AuthorizationPlugin)

	p.authpluginOrder 		= &[]string{}

	//TODO: First make sure that plugindir is actually a directory.

	// List all the files in the plugin directory that match the plugin filename pattern (*.so)
	pluginFiles, err := ioutil.ReadDir(plugindir)
	if err != nil {
		lug.Error("Message", "Failed to open plugin dir", "Directory", plugindir, "Error", err)
	}

	//TODO: Come up with a common set of exit codes?
	//TODO: Return an error here and/or exit?
	if len(pluginFiles) <= 0 {
		lug.Error("Message", "No plugins found in directory", "Directory", plugindir)
		os.Exit(1)
	}

	for _, plugFile := range pluginFiles {
		plugPath := plugindir + "/" + plugFile.Name()
		plug, err := plugin.Open(plugPath)
		if err != nil {
			lug.Error("Message", "Failed to open plugin file", "PluginFile", plugPath, "Error", err)
		}

		wolfSym, err := plug.Lookup("WolfPlugin")

		if err != nil {
			lug.Error("Message", "Failed to load plugin from file", "PluginFile", plugPath, "Error", err)
		}

		wolfPlug, ok := wolfSym.(WolfPlugin)

		if !ok {
			lug.Error("Message", "Failed to load plugin. The specified plugin does not implement WolfPlugin", "PluginFile", plugPath, "Error", err)
		}

		//TODO: I can almost guarantee there is a much better way to do this.
		switch plugType := wolfPlug.GetPluginType(); plugType {
		case PLUGIN_TYPE_NONE:
			lug.Error("Message", "Unknown plugin type", "PluginFile", plugPath,)
		case PLUGIN_TYPE_ACTION:
			p.actionPlugins[wolfPlug.GetPluginName()] = wolfPlug.(ActionPlugin)
			lug.Debug("Message", "Action plugin loaded", "PluginName", wolfPlug.GetPluginName(), "PluginVersion", wolfPlug.GetPluginVersion())

		case PLUGIN_TYPE_LISTENER:
		case PLUGIN_TYPE_PARSER:
		case PLUGIN_TYPE_AUTHORIZATION:
			p.authPlugins[wolfPlug.GetPluginName()] = wolfPlug.(AuthorizationPlugin)
			lug.Debug("Message", "Authorization plugin loaded", "PluginName", wolfPlug.GetPluginName(), "PluginVersion", wolfPlug.GetPluginVersion())

		case PLUGIN_TYPE_AUTHN_SOURCE:
		case PLUGIN_TYPE_ERROR:
		default:
			lug.Error("Message", "Unknown plugin type", "PluginFile", plugPath,)
		}

	}

	//p.authpluginOrder := []string
	// build the order of authorization plugins
	for plugName, _ := range p.authPlugins {
		//TODO: Implement initializing the plugins. Which will mean we need to be passed the plugins config
		//lug.Debug("Message", "Initializing plugin", "PluginName", plugName)
		//*p.authPlugins[plugName].InitPlugin(config plugins.AuthorizationPluginConfig)

		lug.Debug("Message", "Adding authorization plugin to list.", "PluginName", plugName)
		*p.authpluginOrder = append(*p.authpluginOrder, plugName)

	}

	lug.Debug("Message", "Plugin order compiled", "PluginOrder", *p.authpluginOrder)
}

// Returns a reference to a slice comprised of the names of the authorization plugins in the order in which they
// should be called.
// Pretty sure there is a better way to do this.
func (p *PluginManager) GetAuthPluginOrder() *[]string {
	return p.authpluginOrder
}

//TODO: Convert these to pointers instead of actual types so that we aren't duplicating what is probably going to
// turn out to be the largest object type we create in memory.

// Fetch a reference to an Action Plugin by name.
func (p *PluginManager) GetActionPlugin(pluginName string) ActionPlugin {

	return p.actionPlugins[pluginName]
}

//func (p *PluginManager) GetAuthenticationSourcePlugin(pluginName string) authenticationsource.AuthenticationSourcePlugin {
//	return p.authSourcePlugins[pluginName]
//}

// Fetch a reference to an Authorization plugin by name.
func (p *PluginManager) GetAuthorizationPlugin(pluginName string) AuthorizationPlugin {
	return p.authPlugins[pluginName]
}

// Keep going
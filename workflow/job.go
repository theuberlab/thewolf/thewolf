package workflow

import "time"

type Job struct {
	JobID 		string 				`yaml:"JobID"` // Until I come up with something better.
	Labels 		map[string]string 	`yaml:"Labels"`//
	StartTime 	time.Time 			`yaml:"StartTime"`// When the job was kicked off
}

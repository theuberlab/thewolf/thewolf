# About #
You have applications, applications have problems. You have Prometheus to help spot those problems.

Now it's time to call TheWolf to solve them.

The Wolf is a flexible, plugin based problem resolver. It accepts notifications 
(currently only from Prometheus Alertmanager's Webhook Reciever) and triggers 
a configured action based on the alert.

Actions are performed by plugins and can be templetized based on labels
parsed from the alert.

Access to TheWolf is controlled by authorization plugins which can be chained
together for more robust support.

In the future TheWolf will support additional plugin types. See below for more information.

# Project Status
TheWolf now has enough minimum functionality to be worthy of a non-zero 
minor version. 
It works, just don't look at the code if you're eating or anything.

*  Plugin architecture - The basis of the plugin architecture is in place. 
The mechanisms for implementing two types of plugins (Action and Authorization) 
work but definately need further refinement. The mechanisms for plugins 
validating their configurations, for example, are only beginning to be implemented.
*  Action Plugins - Two types of core action plugin are implemented one just 
responds with text and the other executes external commands subject to the above caveats.
*  Authorization plugins - The Authorization Plugin architecture is in 
place. and about as mature as Action plugins. You can chain them together
to a degree. Currently it only works alphabetically. 
There is currently only one authorization plugin, HTTPBasic, which is 
more of an example/POC plugin and not intended for production use. 
At least two more plugins are planned. A client certificate validator
for use with MutalTLS (See below) and a header validator which should
be flexible enough to use with a number of other technologies.
*  MutualTLS - Mutual TLS Authorization is implemented. A third TLS
environment variable, THE_WOLF_AUTHORITY_DATA, has been added. If this
is populated TheWolf will use this (and only this) as an authority pool
when validating client certificates. Like THE_WOLF_PRIVATE_KEY and
THE_WOLF_PRIVATE_KEY the value must be base64 encoded due to limitations
with shell environment variables and newlines.
*  Pipeline - The pipeline is currently broken as the architecture has
changed rapidly and I ahve been a bad codemonkey and not writing new tests.
Making the pipeline more robust (more tests, actually validating against
prometheus, etc.) is the current priority.
*  Authentication Sources - Work on this will begin next after some
pipeline cleanup.
*  Metrics - Yeah...
*  Documentation - Not 100% inacurate and mising woohoo! Documentation
continues to improve. I give it a little attention with every update.
*  Listeners/parsers - Still only one listener/parser which listens for 
prometheus webhook requests. These are fairly low priority ideas.

# Concepts
More detail on these and other concepts can be found in [the design documentation](Documentation/Design/Design.md)

## Plugins
TheWolf is built heavily around plugins. Most functionality is either currently
performed by plugins or will be migrated to plugins in the future. 

Details on the plugin architecture, plugin types and future plans for
plugins can be found in [the plugins documentation](Documentation/Plugins/Plugins.md)

## Authorization
Simply executing a Linux command based on an alertname could be implemented
by anyone with moderate skills in nearly any language. Placing such a tool
on your network would then allow anyone in your organization to execute these
same commands, potentially impacting stability or destroying data.

TheWolf places a heavy emphasis on the Authorization of in-bound requests.
Authorization is implemented through [Authorization plugins](Documentation/Plugins/Authorization.md)
which can be chained together for more or less security.

## Action
[An Action](Documentation/Design/Actions.md) is a definition of some 
activity you wish TheWolf to perform. It is selected by the alert name 
and passed [a job object](Documentation/Design/Jobs.md) which includes 
the labels and annotations parsed from the request as well as the plugin
which will be called and a plugin specific configuration.

## Authentication Sources.
Not yet implemented but these will be used to fetch the details required
if performing the specified action also requires authorization.
I.E. Client certifiates, username/password pairs, etc.


# Contributing
If you wish to contribute to TheWolf there are three main venues. 

1)  For core functionality Fork this repository and issue change requests
1)  For core plugins fork [TheWolf's core plugins](https://bitbucket.org/theuberlab/wolfplugins/) 
repository and issue change requests
1)  Write your own plugin! Please let us know if you intend to begin work
on a plugin so that we can link to your work.


# Experimenting

For those interested in experimenting with TheWolf we provide some tools.

simply run 

```make demo```

This will perform a go get of TheWolf's plugins repository, build TheWolf
and it's plugins and start TheWolf with the e2e test config file.

Then make a curl request like the following:

```curl -kv -H"Authorization: Basic dXNlcjI6cGFzc3dvcmQy" -H "Content-Type: application/json" -X POST https://localhost:8443/webhook --data '{"receiver": "webhook","status": "firing","alerts": [{"status": "firing","labels": {"node": "ip-10.1.2.3.us-west-2.compute.internal","user": "sbjorkensen","team": "platform","severity": "Critical","alertname": "TradWebServiceNotReadyForTooLongJustOne"},"annotations": {"description": "Node ip-10.1.2.3.us-west-2.compute.internal has been in a NOT READY state for more than 9m, this should be investigated.","summary": "Node ip-10.1.2.3.us-west-2.compute.internal has been in a NOT READY state for more than 9m, thats generally not awesome."},"startsAt": "2018-08-03T09:52:26.739266876+02:00","endsAt": "0001-01-01T00:00:00Z","generatorURL": "http://simon-laptop:9090/graph?g0.expr=go_memstats_alloc_bytes+%3E+0\u0026g0.tab=1"}]}'```

Note the e2e test config currently has the HTTPBasic authorization
plugin enabled and configured. This can (and probably should) be disabled
by removing the plugin from the plugins directory and removing the corresponding
plugin config section from TheWolf's config.

Instead of HTTPBasic (or in conjunction with) Mutual TLS authorization 
should be used. See below for directions on configuring TheWolf for MutualTLS.

## TLS
TheWolf cannot be run without TLS. At startup it will check the presence of
and content of two environment variables ```THE_WOLF_PRIVATE_KEY```
and ```THE_WOLF_CERTIFICATE```.

Due to limitations with environment variables and newlines the values must 
be base64 encoded.

If these variables are are not found OR if they do not contain a base64
encoded private key and certificate pair TheWolf will fall back and
generate a key and self-signed certificate.

This key and cert pair will be re-generated each time TheWolf is launched.


## MutualTLS
To enable mutual TLS authorization simply provide the certificate required
to validate the client certificates you will use in a third TLS variable, 
```THE_WOLF_AUTHORITY_DATA```.

Again this must be base64 encoded.

To simplify testing with MutualTLS a stand alone tool is provided in
```cmd/certGenerator/certGenerator.go``` which will generate 3 
certificate/key pairs and writes them to the current working directory. 

certGenerator produces a CA certificate as well as client
and server certificates signed by it.

To experiment with TheWolf using mutual TLS authorization try the following

```
go run cmd/certGenerator/certGenerator.go
export THE_WOLF_PRIVATE_KEY=`cat server.key | base64`
export THE_WOLF_CERTIFICATE=`cat server.crt | base64`
export THE_WOLF_AUTHORITY_DATA=`cat ca.crt | base64`
```

Then in another window run:
```curl -kv --key client.key --cert client.crt -H "Content-Type: application/json" -X POST https://localhost:8443/webhook --data '{"receiver": "webhook","status": "firing","alerts": [{"status": "firing","labels": {"node": "ip-10.1.2.3.us-west-2.compute.internal","user": "sbjorkensen","team": "platform","severity": "Critical","alertname": "TradWebServiceNotReadyForTooLongJustOne"},"annotations": {"description": "Node ip-10.1.2.3.us-west-2.compute.internal has been in a NOT READY state for more than 9m, this should be investigated.","summary": "Node ip-10.1.2.3.us-west-2.compute.internal has been in a NOT READY state for more than 9m, thats generally not awesome."},"startsAt": "2018-08-03T09:52:26.739266876+02:00","endsAt": "0001-01-01T00:00:00Z","generatorURL": "http://simon-laptop:9090/graph?g0.expr=go_memstats_alloc_bytes+%3E+0\u0026g0.tab=1"}]}'```

NOTE: Don't forget to clean up the 6 files you just generated when you are finished.


## Config
The config file in the e2e tests directory can be used as an example.

## Building
To build make sure you have fetched both this and the [wolf plugins](https://bitbucket.org/theuberlab/wolfplugins/)
repository.

Then run:

```make thewolf```

Which will give you the server and plugin binaries in the release directory.

Then run TheWolf providing a config file and path to the plugins directory.

I.E.

```
./release/thewolf-0.1.1-darwin-amd64 -c ./tests/e2eTests/thewolfconfig.yml -P ./release/plugins/darwin/
```

# Known Limitations
*  Because golang does not currently support plugins for Windows TheWolf
is supported only on Linux and MacOS (or any Unix descendent operating system
you can get it to build on.)
*  Due to the nature of the golang plugin architecture cross-compilation
is not as simple as ```GOOS=linux GOARCH=amd64 go build``` A C cross 
compiler would need to be installed. In theory the current usage of bazel
might mitigate this but A) it's bazel (see below) and B) I don't 
readily have directions on how to make it work.



# Bazel
To solve a major dependency nightmare I've had to temporarily build
with [Bazel.](https://bazel.build/) We need to get out from under that 
because as neat as it might be under the hood, bazel is a nightmare to 
manage and I fear it will turn off potential contributors.

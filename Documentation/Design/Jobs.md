# Jobs
Currently not terribly well thought out. A job is an activity triggered
upon recieving a request from an external system. 

Currently a job is only a short lived internal object which includes 
the configuration of an action as well as the labels and annotations
parsed from the webhook request.

It is expected that later a job will become a more complex workflow type
object with attributes such as run time, phase, and more.
# Actions

An Action is a definition of some activity you wish TheWolf to perform
in response to an alert notification. It is selected by the alert name 
and passed [a job object](Documentation/Design/Jobs.md) which includes 
the labels and annotations parsed from the request as well as the plugin
which will be called and a plugin specific configuration.

It is still a little bit up in the air what all an Action needs to 
support but at minimum.

*  An alert name        The alert name parsed from the request
*  An action plugin     The plugin to be called
*  Action Configuration The config to pass to the plugin which tells it
what activity to perform (a specific external command to execute, a
node to delete, etc)
*  Concurrency          Should it be allowed to run concurrently, since
this is not yet implemented there are a few options for how we do so.
*  Auth Details         Any authorization details that need to be fetched
in order to perform the activity (I.E. Fetch a specific clientSSL certificate
and key from a file store auth source plugin.)

Additional Items we are likely to support in the future

*  Failure Action(s)    An optional action to call upon the event of a 
failure. There are a number of possible "failure" cases (execution error,
timeout, max retries exceeded, more) so it's unclear if we should support 
several explicitely or make this a part of the action config left up to 
the plugin itself. 
*  Rollback Action(s)   Pretty much the same situation as the above.

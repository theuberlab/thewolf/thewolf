# Action Plugins
Action plugins are the part of TheWolf which actually do something.
This could be the execution of an external command or interaction
with an API such as Kubernetes or AWS.

They are expected to support templating via [Golang's internal templating support](https://golang.org/pkg/text/template/)

TheWolf passes [a job object](Documentation/Design/Jobs.md) to the action
plugin which includes the configuration for the triggered action as well
as a map of labels. This currently includes all labels provided by prometheus
in the webhook request as well as the annotations.

So a request such as this:

```
{
  "receiver": "webhook",
  "status": "firing",
  "alerts": [
    {
      "status": "firing",
      "labels": {
        "team": "platform",
        "user": "sbjorkensen",
        "severity": "Critical",
        "node": "node1.us-west-2.compute.internal",
        "alertname": "NodeNotReadyForTooLongJustOne"
      },
      "annotations": {
        "description": "Node node1.us-west-2.compute.internal has been in a NOT READY state for more than 9m, this should be investigated.",
        "summary": "Node node2.us-west-2.compute.internal has been in a NOT READY state for more than 9m, thats generally not awesome."
      },
      "startsAt": "2018-08-03T09:52:26.739266876+02:00",
      "endsAt": "0001-01-01T00:00:00Z",
      "generatorURL": "http://simon-laptop:9090/graph?g0.expr=go_memstats_alloc_bytes+%3E+0&g0.tab=1"
    }
  ]
}
```

Would trigger an action configured for the alert named 'NodeNotReadyForTooLongJustOne'
and include the labels

```
{
    "team": "platform",
    "severity": "Critical",
    "alertname": "NodeNotReadyForTooLongJustOne",
    "annotation.description": "Node node1.us-west-2.compute.internal has been in a NOT READY state for more than 9m, this should be investigated.",
    "annotation.summary": "Node node1.us-west-2.compute.internal has been in a NOT READY state for more than 9m, thats generally not awesome.",
}
```

The value 'sbjorkensen', for example, would be accessed within the plugin configuration
by the template syntax '{{.Labels.user}}'.

For example given the above request and an action configured as follows:

```
NodeNotReadyForTooLongJustOne:
  Plugin: "ExternalCommandExecutor"
  Comments: When only one node has been down too long we can just kill it off.
  Config:
    Command: "kubectl"
    Args:
    - "--kubeconfig=/thewolf/.kube/config"
    - "--context=cluster1"
    - "delete"
    - "node {{.Labels.user}}"
  Alert: "NodeNotReadyForTooLongJustOne"
```

would call the ExternalCommandExecutor plugin in turn which would issue the command:

```kubectl --kubeconfig=/thewolf/.kube/config --context=cluster1 delete node node1.us-west-2.compute.internal```

Currently there are two Action plugins implemented.

The TEXTResponder plugin which simply responds with configured text.

The ExternalCommandExecutor plugin which executes an external linux command.
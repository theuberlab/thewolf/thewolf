package model

import "time"

//TODO: pull this in automatically from the prometheus code.

// Data is the data passed to notification templates and webhook pushes.
//
// End-users should not be exposed to Go's type system, as this will confuse them and prevent
// simple things like simple equality checks to fail. Map everything to float64/string.
//
// swagger:parameters HandleWithAuthorization
type WebhookData struct {
	// in: body
	// Required: false
	// Example: "webhook"
	Receiver string `json:"receiver"`
	// in: body
	// Required: true
	// Exmple: "firing"
	Status   string `json:"status"`
	// in: body
	// Required: true
	Alerts   Alerts `json:"alerts"`
	// in: body
	// Required: false
	GroupLabels       KV `json:"groupLabels"`
	// in: body
	// Required: false
	CommonLabels      KV `json:"commonLabels"`
	// in: body
	// Required: false
	CommonAnnotations KV `json:"commonAnnotations"`
	// in: body
	// Required: false
	ExternalURL string `json:"externalURL"`
}

// Alerts is a list of Alert objects.
//
// swagger:model
type Alerts []Alert

// Alert holds one alert for notification templates.
//
// swagger:model
type Alert struct {
	// the id for this user
	//
	// required: true
    // example: "firing"
	Status       string    `json:"status"`
	// the id for this user
	//
	// required: true
	// Minimum items: 1
	// Minimum items: 60
	// Unique: true
	// example: {"team": "platform","severity": "Critical","alertname": "NodeNotReadyForTooLongJustOne"}
	Labels       KV        `json:"labels"`
	// required: true
	// Minimum items: 1
	// Minimum items: 60
	// Unique: true
	// example: {"description": "Node ip-10.1.2.3.us-west-2.compute.internal has been in a NOT READY state for more than 9m, this should be investigated.","summary": "Node ip-10.1.2.3.us-west-2.compute.internal has been in a NOT READY state for more than 9m, thats generally not awesome."}
	Annotations  KV        `json:"annotations"`
	// required: true
	// example: 2018-08-03T09:52:26.739266876+02:00
	StartsAt     time.Time `json:"startsAt"`
	// required: true
	// example: 0001-01-01T00:00:00Z
	EndsAt       time.Time `json:"endsAt"`
	// required: false
	// example: http://simon-laptop:9090/graph?g0.expr=go_memstats_alloc_bytes+%3E+0\u0026g0.tab=1
	GeneratorURL string    `json:"generatorURL"`
}

// KV is a set of key/value string pairs.
//
// swagger:model
type KV map[string]string
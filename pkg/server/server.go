package server

import (
	"bitbucket.org/theuberlab/thewolf/pkg/model"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"bitbucket.org/theuberlab/lug"
	"bitbucket.org/theuberlab/thewolf/config"
	"bitbucket.org/theuberlab/thewolf/plugins"
	"bitbucket.org/theuberlab/thewolf/workflow"
	"github.com/gorilla/mux"
)


type Server struct {
	PluginManager 	*plugins.PluginManager
	Router			*mux.Router
	WolfConfig 		*config.Configuration
}

//type WebhookHandler struct {
//	wolfConfig    *config.Configuration
//	pluginManager *plugins.PluginManager
//}

//TODO: I don't really like this format. At least add a parsable error field. Probably want a command output section?
type responseJSON struct {
	Status  int
	Message string
}


//TODO: Understand this better.

// I don't fully understand how this bit works but it's part of some cross-site scripting protection I got from a friend.
var corsHeaders = map[string]string{
	"Access-Control-Allow-Headers":  "Accept, Authorization, Content-Type, Origin",
	"Access-Control-Allow-Methods":  "GET, OPTIONS",
	"Access-Control-Allow-Origin":   "*",
	"Access-Control-Expose-Headers": "Date",
}

func GetWolfServer(pluginManager *plugins.PluginManager, config *config.Configuration) Server {
	return Server{
		PluginManager: 	pluginManager,
		Router: 		mux.NewRouter().StrictSlash(true),
		WolfConfig: 	config,
	}
}

// Enables cross-site script calls.
func (s *Server) setCORS(w http.ResponseWriter) {
	for h, v := range corsHeaders {
		w.Header().Set(h, v)
	}
}

// Creates and returns reference a gorillamux router
func (s *Server) GetRouter() *mux.Router {
	return s.Router
}


//TODO: Make this return an example body which can be used in a POST
//       OR
//TODO: Write a /webhook handler which lists configured webhooks. This would, or course, require authentication to be enabled.
// A main handler which lug.Logs when it was accessed and responds with "Sample App"
func (s *Server) handleUsage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// Log the request to STDOUT

		lug.Warn("Message", "WebHookUsage called.")
		// Set CSS headers
		s.setCORS(w)

		// Respond with a 200
		w.WriteHeader(http.StatusOK)
		message := fmt.Sprintf("Nothing here yet but in the future we will tell you how to format a POST to this same endpoint.")
		io.WriteString(w, message)
	}

}

// A main handler which lug.Logs when it was accessed and responds with "Sample App"
func (s *Server) handleRoot() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// Log the request to STDOUT

		lug.Warn("Message", "/ called at %s\n", time.Now())
		// Set CSS headers
		s.setCORS(w)

		// Respond with a 200
		w.WriteHeader(http.StatusOK)
		message := fmt.Sprintf("Nothing here, perhaps you meant <a href=\"/webhook\"")
		io.WriteString(w, message)
	}


}



// An example of how to pass a method to something that validates it first. So the authorizationplugins will implement
// a method like this, passing the actual handler function (which will ultimately be implemented by a listener) to one
// another and then returning it. Example comes from here: https://medium.com/statuscode/how-i-write-go-http-services-after-seven-years-37c208122831
func (s *Server) adminOnly(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		//if !currentUser(r).IsAdmin {
		//	http.NotFound(w, r)
		//	return
		//}
		//h(w, r)
	}
}

//func (s *Server) HandleWithAuthorization(handlerFunc http.HandlerFunc, authPlugins ...func(http.HandlerFunc) http.HandlerFunc) http.HandlerFunc {

//func (s *Server) HandleWithAuthorization(handlerFunc http.HandlerFunc, authPlugins ...plugins.AuthorizationPlugin) http.HandlerFunc {

func (s *Server) HandleWithAuthorization(handlerFunc http.HandlerFunc, authPlugins ...string) http.HandlerFunc {

	for _, authorizor := range authPlugins {
		authorizorConfig := s.WolfConfig.Plugins.Authorization[authorizor]

		//handlerFunc = s.PluginManager.GetAuthorizationPlugin(authorizor).GetAuthorizationFunction()(handlerFunc)
		handlerFunc = s.PluginManager.GetAuthorizationPlugin(authorizor).GetAuthorizationFunctionWithConfig(authorizorConfig)(handlerFunc)


		//handlerFunc = authorizor(handlerFunc)

		//actually
		//authorizor.GetAuthorizationFunction()  func(http.HandlerFunc)
	}


	return handlerFunc
}

func (s *Server) HandlePrometheusRequest() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		defer r.Body.Close()

		//TODO: Add a 402 Payment Required response if theres no body
		// Godoc: https://godoc.org/github.com/prometheus/alertmanager/template#Data

		data := model.WebhookData{}

		if err := json.NewDecoder(r.Body).Decode(&data); err != nil {

			lug.Error("Message", "Failed to parse request body", "Error", err)

			response := responseJSON{
				Status:  http.StatusBadRequest,
				Message: fmt.Sprintf("Failed to parse request body. Error: %v", err),
			}

			responseBytes, err := json.Marshal(response)
			if err != nil {
				lug.Error("Message", "Failed to convert response to json.", "Error", err)
				//TODO: Return a 500 here.
			}

			http.Error(w, string(responseBytes), http.StatusBadRequest)

			return
		}
		lug.Trace("Message", "Got data", "Data", fmt.Sprintf("%v", data))

		for _, alert := range data.Alerts {
			lug.Debug("Alert: ", alert)
			severity := alert.Labels["severity"]
			alertName := alert.Labels["alertname"]

			lug.Error("Message", "Got webhook request", "Alertname", alertName, "Severity", severity)

			alertAction, ok := s.WolfConfig.Actions[alertName]

			if ok {

				jobLabels := make(map[string]string)

				jobLabels["status"] = alert.Status

				jobLabels["startsAt"] = fmt.Sprintf("%s", alert.StartsAt)
				jobLabels["endsAt"] = fmt.Sprintf("%s", alert.EndsAt)
				jobLabels["generatorURL"] = alert.GeneratorURL

				for key, value := range alert.Labels {
					jobLabels[key] = value
				}

				for key, value := range alert.Annotations {
					jobLabels["annotation."+key] = value
				}

				wfJob := workflow.Job{
					JobID:     "1", // Number ONE YEAH!
					Labels:    jobLabels,
					StartTime: time.Now(),
				}

				instance := plugins.ActionInstance{
					Config: alertAction,
					Job:    wfJob,
					Labels: map[string]string{"InputPlugin": "Webhook"},
				}

				lug.Debug("Message", "Calling TriggerAction on plugin", "PluginName", alertAction.Plugin, "ActionInstance", fmt.Sprintf("%v", instance), "Job", fmt.Sprintf("%v", instance.Job))

				pluginResults := s.PluginManager.GetActionPlugin(alertAction.Plugin).TriggerAction(instance)

				lug.Debug("Message", "Got response from plugin", "Results", pluginResults)
				response := responseJSON{
					Status:  http.StatusOK,
					Message: fmt.Sprintf("Success. Results: %s\n", pluginResults),
				}

				json.NewEncoder(w).Encode(response)
			} else {
				lug.Error("Message", "Found action for unknown alert")

				response := responseJSON{
					Status:  http.StatusNotFound,
					Message: fmt.Sprintf("Unknown alert name. Alert: %v", alertName),
				}

				responseBytes, err := json.Marshal(response)
				if err != nil {
					lug.Error("Message", "Failed to convert response to json.", "Error", err)
					//TODO: Return a 500 here.
				}

				http.Error(w, string(responseBytes), http.StatusNotFound)

				return
			}

		}
	}
}

// A main handler which lug.Logs when it was accessed and responds with "Sample App"
func (s *Server) swaggerSpec() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// Log the request to STDOUT

		lug.Warn("Message", "swagger.json called at %s\n", time.Now())
		// Set CSS headers
		s.setCORS(w)

		// Respond with a 200
		w.WriteHeader(http.StatusOK)
		message := fmt.Sprintf(swaggerSpec)
		io.WriteString(w, message)
	}


}
package config

import (
	"encoding/json"
	"fmt"

	"github.com/ghodss/yaml"
	"github.com/toolkits/file"
	"bitbucket.org/theuberlab/lug"
	"bitbucket.org/theuberlab/thewolf/plugins"
)

var (
	config		Configuration
)

type Configuration struct {
	Plugins 		plugins.PluginsConfig 				`yaml:"Plugins,omitempty"` 	// _probably_ want to be able to omit this? Maybe plugins is required because authorizationplugins are required but the other types are omitempty? No need to require that they configure plugins if they just want to include them in the plugins dir and use their default settings.
	Actions 		map[string]plugins.ActionConfig 	`yaml:"Actions"`			// Later perhaps we define "jobs" where the job defines the entire flow. Right now we're only talking about configuring individual actions which may or may not call other actions (once that is implemented.)
}

//type PluginsConfig struct {
//	map[string]plugins.PluginsConfig
//}
//type Action struct {
//	ActionScript	ActionScript 	`yaml:"ActionScript"`
//	Alert        	string 		`yaml:"Alert"`
//	Concurrency  	string 		`yaml:"Concurrency"`
//}

//type ActionScript struct {
//	Command			string 			`yaml:"String"`
//	Args 			[]string 		`yaml:"Args"`
//}

// Pull the file from the disk
func readFile(fileName string) (string, error) {
	// This should only be called if we have been passed a file name. Die.
	if fileName == "" {
		return "", fmt.Errorf("No file provided to readFile")
		//os.Exit(1)
	}

	// Check to see that the file exists.
	if !file.IsExist(fileName) {
		return "", fmt.Errorf("config file %s does not exist", fileName)
		//os.Exit(1)
	}

	content, err := file.ToTrimString(fileName)
	if err != nil {
		return "", fmt.Errorf("Error reading file: %s Error: %v", fileName, err)
	}

	return content, nil
}

// load the config file and parse it into the config
func LoadConfig(filename string, configRef *Configuration) (error) {
	var (
		content string
		err		error
	)

	content, err = readFile(filename)

	if err != nil {
		lug.Error("Message", "LoadConfig: Could not read file.", "Filename", filename, "Error", err)
		//InitDefaultLogger()
		return err
	}

	// Let's first convert it to JSON since that's what yaml.Unmarshal does anyway.
	yamlBytes, err := yaml.YAMLToJSON([]byte(content))
	if err != nil {
		lug.Error("Message", "LoadConfig: Could not convert from yaml to json.", "Filename", filename, "Error", err)
		return err
	}

	err = json.Unmarshal(yamlBytes, &config)

	lug.Debug("Message", "Parsed Yaml for JSON", "JSON", yamlBytes)
	lug.Trace("Message", "Unmarshaled Config", "Config", fmt.Sprintf("%v", config))
	//err = yaml.Unmarshal([]byte(content), config)
	if err != nil {
		lug.Error("Message", "LoadConfig: Could not parse yaml configuration file.", "Filename", filename, "Error", err)
		return err
	}

	for _, item := range config.Actions {
		lug.Error("Message", "Parsed Action config", "Plugin", item.Plugin, "Alert", item.Alert, "Action Config", item.Config, "Timeout", item.Timeout, "Concurrency", item.Concurrency)

	}

	*configRef = config
	return nil
}


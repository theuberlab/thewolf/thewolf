package config

import (
	"strings"
)

type Concurrency int

const (
	ERROR			Concurrency = 0	//
	ALLOW			Concurrency = 1	//
	BLOCK			Concurrency = 2	//
	QUEUE			Concurrency = 3	// Not yet implemented
	UNKNOWN			Concurrency = 500	// For certain unhealthy states
)


// Gets the string representation of a Concurency
func (l *Concurrency) String() string {
	names := [...]string{
		"ERROR",
		"ALLOW",
		"BLOCK",
		"QUEUE",
		"UNKNOWN",
	}

	if *l < ERROR || *l > UNKNOWN {
		return "ERROR"
	}

	return names[*l]
}

// Used to convert a human readable string like "Allow" or "allow" to a Concurrency type.
func getConcurrencyFromString(levelString string) (Concurrency) {
	enums := make(map[string]Concurrency)

	enums["ERROR"]		= ERROR
	enums["ALLOW"]		= ALLOW
	enums["BLOCK"]		= BLOCK
	enums["QUEUE"]		= QUEUE
	enums["UNKNOWN"]	= UNKNOWN

	levelString = strings.ToUpper(levelString)

	result, ok := enums["foo"]

	if ok {
		return result
	}

	return enums["ERROR"]
}

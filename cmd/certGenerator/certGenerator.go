package main

import (
	"crypto/x509"
	"encoding/pem"
	"os"

	"bitbucket.org/theuberlab/lug"
	"bitbucket.org/theuberlab/thewolf/pkg/TLS"
)



func main() {
	// Generate the CA key and certificate
	caKey, caCertPem := TLS.GenKeyCertPair("ca", nil, nil)
	caBlock, _ := pem.Decode([]byte(caCertPem))

	if caBlock == nil {
		lug.Error("Message", "failed to decode certificate PEM")
		os.Exit(1)
	}

	caCert, err := x509.ParseCertificate(caBlock.Bytes)

	if err != nil {
		lug.Error("Message", "failed to parse server certificate", "Error", err)
		os.Exit(1)
	}

	TLS.WriteFiles(caKey, caBlock.Bytes, "ca")


	// Generate the server key and certificate
	serverkey, serverCertPem := TLS.GenKeyCertPair("server", caCert, caKey)
	serverBlock, _ := pem.Decode([]byte(serverCertPem))
	if caBlock == nil {
		lug.Error("Message", "failed to decode certificate PEM")
		os.Exit(1)
	}

	if err != nil {
		lug.Error("Message", "failed to parse server certificate", "Error", err)
		os.Exit(1)
	}

	TLS.WriteFiles(serverkey, serverBlock.Bytes, "server")


	// Generate the client key and certificate
	clientkey, clientCertPem := TLS.GenKeyCertPair("client", caCert, caKey)
	clientBlock, _ := pem.Decode([]byte(clientCertPem))

	if caBlock == nil {
		lug.Error("Message", "failed to decode certificate PEM")
		os.Exit(1)
	}

	if err != nil {
		lug.Error("Message", "failed to parse server certificate", "Error", err)
		os.Exit(1)
	}

	TLS.WriteFiles(clientkey, clientBlock.Bytes, "client")
}
